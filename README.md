# Date Calculator
************************************************************
**				Date Calculator						      **
**														  **
**					By								      **
**														  **
**				Michael Sorouni							  **
************************************************************

The date calculator is implemented as a bash script using the Bash Unix shell command language.

I decided to write a Bash script to be able to run it as a unix command so as to avoid the need to install a runtime environment such as JRE 
or any other binary distribution package to run a specific language.  

Running the script:

1. Copy the sh file to a specific location.
2. From the command line, go to the file location where you copied the script
and enter the fllowing command:
	$ sh days.sh

Design Purpose and Reasons:

The script calculates the number of days between two dates entered by the user.
It calculates the number of days by working out the number of days from year 0 (i.e 00/00/0000)
including leap years for each date entered and then subtracts the results to give the difference
as the answer.

