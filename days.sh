#!/bin/bash

#************************************************************
#**				    Date Calculator						  **
#**														  **
#**					      By							  **
#**														  **
#**				    Michael Sorouni						  **
#************************************************************

#applies validation on date using yyyy-mm-dd format
function isDateValid {
    DATE=$1

	if [[ $DATE =~ ^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$ ]]; then		
		return 0
	else			
		return 1
	fi
}

readFromDate () {

	echo "Type the from date (yyyy-mm-dd) and [ENTER]:"

	read dateFrom
	
	if isDateValid $dateFrom; then
		IFS='-' tokensFrom=( $dateFrom )
		yearFrom=${tokensFrom[0]}
		monthFrom=${tokensFrom[1]}
		dayFrom=${tokensFrom[2]}
		
		#check the year is from 1901
		if  [ ! $yearFrom -ge 1901 ] 
		then
			echo "The year must be from 1901"
			readFromDate
		else
			date1=($yearFrom $monthFrom $dayFrom)
		fi

		IFS=''		
	else
		echo "Date not in valid format. Please try again"
		readFromDate
	fi	
}

readToDate () {

	echo "Type the to date (yyyy-mm-dd) and [ENTER]:"
	
	read dateTo

	if isDateValid $dateTo; then
		IFS='-' tokensTo=( $dateTo )

		yearTo=${tokensTo[0]}
		monthTo=${tokensTo[1]}
		dayTo=${tokensTo[2]}
		
		if  [ ! $yearTo -le 2999 ] 
		then
			echo "The year must not be more than 2999"
			readToDate
		else
			date2=($yearTo $monthTo $dayTo)	
		fi	
		IFS=''
	else
		echo "Date not in valid format. Please try again"
		readToDate
	fi		
}

#calculates the number of leap years
calcNumLeapYears () {
	years=$1 
	#check if year needs to be included in leap year count
	if  [ $2 -le 2 ] 
	then
		years=$(($years-1))
	fi	
	
	#add leaps year if it is a divisible by 4, 400 and not divisible by 100
	echo $(($years/4 - $years/100 + $years/400))
}

calDateDiff () {

	#calculate days for dateFrom from 00/00/0000
	dateFrom=$1[@]
	
	a1=("${!dateFrom}")
	COUNTER=0
    for i in "${a1[@]}" ; do

		if [ $COUNTER -eq 0 ]
		then 
			yearFrom=$i 
		fi
		if [ $COUNTER -eq 1 ]
		then 
			monthFrom=$i 
		fi
		if [ $COUNTER -eq 2 ]
		then 
			dayFrom=$i 
		fi
		let COUNTER=COUNTER+1 
    done
	
	# calculate days using years from 00/00/0000 and day
	numDays1=$(($yearFrom*365 + 10#$dayFrom))

	# Add days for month
	for ((i=0; i<10#$monthFrom - 1; i++))
	do
        numDays1=$(($numDays1 + ${months[i]}))
	done	

	#add a day for each leap year
	numDays1=$(($numDays1 + $(calcNumLeapYears $yearFrom $monthFrom $dayFrom)))
		
	#calculate days for dateTo from 00/00/0000
	dateTo=$2[@]
	
	a1=("${!dateTo}")
	COUNTER=0
    for i in "${a1[@]}" ; do
  
		if [ $COUNTER -eq 0 ]
		then 
			yearTo=$i 
		fi
		if [ $COUNTER -eq 1 ]
		then 
			monthTo=$i 
		fi
		if [ $COUNTER -eq 2 ]
		then 
			dayTo=$i 
		fi
		let COUNTER=COUNTER+1 
    done
	
	# calculate days using years from 00/00/0000 and day
	numDays2=$(($yearTo*365 + 10#$dayTo))

	# Add days for month
	for ((i=0; i<10#$monthTo - 1; i++))
	do
        numDays2=$(($numDays2 + ${months[i]}))
	done	

	#add a day for each leap year
	numDays2=$(($numDays2 + $(calcNumLeapYears $yearTo $monthTo $dayTo)))
	
	echo Number of days is $(($numDays2 - $numDays1 - 1))
	
}

months=(31 28 31 30 31 30 31 31 30 31 30 31)

readFromDate
readToDate
calDateDiff date1 date2
